package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        try{
            if (y.equals(x)) return true;
            if (x.size() == 0) return true;
            if (x.size() > y.size()) return false;

            int n = x.size();
            int k = y.size();

            while ((n > 0) && (k > 0) ){
                if (x.get(n-1).equals(y.get(k-1))){
                    n -= 1;
                    k -= 1;
                }
                else {
                    k -= 1;
                }
            }
            if (n == 0) return true;
            else return false;
        }catch(NullPointerException e){
            throw new IllegalArgumentException();
        }
    }
}
