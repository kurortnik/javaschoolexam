package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import static java.lang.Math.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

    	try {
            inputNumbers.sort(Comparator.comparing(Integer::valueOf));
        }catch (NullPointerException e){
            throw new CannotBuildPyramidException();
        }catch (OutOfMemoryError e){
            throw new CannotBuildPyramidException();
        }

        inputNumbers.sort(Comparator.comparing(Integer::valueOf));
        int[][] matrix;

        if (flag(inputNumbers.size())){
            int size = inputNumbers.size();
            int rows = (int)numberOfRows(size);
            int columns = 2*rows - 1;

            //Заполнение матрицы нулями
            matrix = new int[rows][columns];//Задаем размерность матрице
            for (int[] row : matrix) {
                Arrays.fill(row, 0);
            }

            //заполнение числами, формируем треугольник
            int center = (columns / 2);//Находим центральную точку матрицы
            int count = 1; // сколько чисел будет в строке
            int arrIdx = 0; // индекс массива

            for (int i = 0, offset = 0; i < rows; i++, offset++, count++) {
                int start = center - offset;
                for (int j = 0; j < count * 2; j +=2, arrIdx++) {
                    matrix[i][start + j] = inputNumbers.get(arrIdx);
                }
            }
        }//Выбрасываем исключение
        else {
            throw new CannotBuildPyramidException();
        }

        return matrix;
    }

    public boolean flag(int n){
        double m = (sqrt(8*n+1) - 1) / 2;
        if ((10.0/Math.round(m))==(10/m))  return true;
        else return false;
    }
    public double numberOfRows(int n){
        double m = (sqrt(8*n+1) - 1) / 2;
        return m;
    }
        
}
