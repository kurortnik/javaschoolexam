package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;


public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (errorCheck(statement))return null;
        //проверка в ответе есть ли дробная часть или ответ целый
        else {          
            double ans = reverceExpression(changeExpression(statement));
            if (Double.isInfinite(ans)) return null;

            if ((10.0/(int)ans)==(10/ans)){
                int a = (int)ans;
                return(Integer.toString(a));
            }
            else return(Double.toString(ans));
        }

    }

    //Преобразование к ОПЗ
    private static String changeExpression(String input){
        String output = "";
        Stack<Character> stack = new Stack<Character>();

        for(int i=0; i < input.length(); i++){
            if (Character.isDigit(input.toCharArray()[i])){
                while (!isOperator(input.toCharArray()[i])){
                    output += input.toCharArray()[i];
                    i++;
                    if (i == input.length()) break;
                }
                output += " ";
                i--;
            }

            if(isOperator(input.toCharArray()[i])){
                if (input.toCharArray()[i] == '('){
                    stack.push(input.toCharArray()[i]);
                }
                else if(input.toCharArray()[i] == ')'){
                    char c = stack.pop();
                    while (c != '('){
                        output += c + " ";
                        c = stack.pop();
                    }
                }
                else {
                    if(!stack.empty()) {
                        if (priority(input.toCharArray()[i]) <= priority(stack.peek())) {
                            output += stack.pop() + " ";
                        }
                    }
                    stack.push(input.toCharArray()[i]);
                }
            }

        }
        while (!stack.empty()){
            output += stack.pop() + " ";
        }
        return output;
    }

    //Обратное преобразование
    private static double reverceExpression(String input){
        double result = 0;
        Stack<Double> stack = new Stack<>();

        for(String token : input.split(" ")){
            if(isOperator(token.toCharArray()[0])){
                double a = stack.pop();
                double b = stack.pop();
                switch (token.toCharArray()[0]){
                    case '+':
                        result = b + a;
                        break;
                    case '-':
                        result = b - a;
                        break;
                    case  '*':
                        result = b * a;
                        break;
                    case '/':
                        result = b / a;
                        break;
                }
                stack.push(result);
            }
            else {
                stack.push(Double.parseDouble(token));
            }
        }
        return stack.peek();
    }
    //Проверка входной строки на адекватность
    private static boolean errorCheck(String input){

        if (input == null) return true;
        if (input.equals("")) return true;

        for (int i = 0; i < input.length() - 1; i++){

            //Проверка на 2 подряд идущих "оператора"
            if (isOperator_forErrors(input.toCharArray()[i]) && (isOperator_forErrors(input.toCharArray()[i + 1]))){
                return true;
            }
            //Проверка, что нет ситуциий подряд идущей открывающей скобки и оператора
            if ((input.toCharArray()[i] == '(') && (isOperator_forErrors(input.toCharArray()[i + 1]))){
                return true;
            }

            //Проверка что нет ситуаций подряд удщих числа и открывающей скобки
            if ((input.toCharArray()[i+1] == '(') && (Character.isDigit(input.toCharArray()[i]))){
                return true;
            }

            //Проверка, что нет ситуциий подряд идущей закрывающей скобки и числа
            if ((input.toCharArray()[i] == ')') && (Character.isDigit(input.toCharArray()[i + 1]))){
                return true;
            }

            //Проверка, что нет ситуциий подряд идущего оператора и закрывающей строчки
            if ((input.toCharArray()[i+1] == ')') && (isOperator_forErrors(input.toCharArray()[i]))){
                return true;
            }

            //Проверка на идущие подряд закрывающая скобка и открывающая
            if ((input.toCharArray()[i] == ')') && (input.toCharArray()[i+1] == '(')){
                return true;
            }
        }

        //Проверка есть ли в записи не допустимый символ
        for (Character c : input.toCharArray()){
            if ("+-*/().1234567890".indexOf(c) == -1) return true;
        }

        //Проверка сбалансированности строки скобками
        Stack<Character> stack = new Stack<>();
        for(char c : input.toCharArray()){
            if (c == '('){
                stack.push(c);
            }
            else if (c == ')'){
                if (stack.empty()) return true;
                else {
                    stack.pop();
                }
            }
        }
        if (!stack.empty()) return true;

        return false;
    }

    //Функция для определения "оператор" или не "оператор" для преобразования к ОПЗ
    private static boolean isOperator(char c)
    {
        if ("+-*/()".indexOf(c) != -1) {
            return true;
        }
        return false;
    }

     //Функция для определения "оператор" или не "оператор" (проверятся те, которые не могут идти подряд)
    private static boolean isOperator_forErrors(char c)
    {
        if ("+-*/.".indexOf(c) != -1) {
            return true;
        }
        return false;
    }

    //приоритет операторов для преобразования к ОПЗ
    private static Integer priority(char c){
        switch (c){
            case '(':
                return 0;
            case ')':
                return 1;
            case '+':
                return 2;
            case '-':
                return 2;
            case '*':
                return 3;
            case '/':
                return 3;
            default: return 6;
        }
    }

}
